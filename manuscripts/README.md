# phd-code/manuscripts

These are PDF scans of handwritten notes, made very early on.
These manuscripts are known to contain errors;
e.g. in [radiation-2-line.pdf] > Page 6 >
three lines above equation (r2.23a) > last fraction >
denominator > square root: "O(ξ)" should read "O(ξ²)".

For better writing, see [the actual thesis].

[radiation-2-line.pdf]: radiation-2-line.pdf
[the actual thesis]: https://github.com/yawnoc/phd-thesis
